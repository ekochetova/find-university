﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace FindUniversity
{
    public class Course
    {
        public string Name { get; set; }
        public int Mark { get; set; }
        public List<string> Subjects { get; set; } = new List<string>();
    }
}