﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace FindUniversity
{
    class ViewModel
    {
        // https://stackoverflow.com/questions/28373643/wpf-combobox-binding-itemssource
        public ObservableCollection<string> Subjects { get; private set; }

        public ViewModel(ObservableCollection <string> subjects)
        {
            Subjects = subjects;
        }
    }
}
