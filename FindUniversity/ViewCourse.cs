﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FindUniversity
{
    class ViewCourse
    {
        public ViewCourse(Course course)
        {
            Name = course.Name;
            Mark = course.Mark;
            foreach (var i in course.Subjects)
                Subjects += i + ", ";
            Subjects = Subjects.Substring(0, Subjects.Length - 2);
        }

        public string Name { get; set; }
        public int Mark { get; set; }
        public string Subjects { get; set; }
    }
}
