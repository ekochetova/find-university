﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FindUniversity
{
    class ExamResult
    {
        public ExamResult(string subject, int mark)
        {
            Subject = subject;
            Mark = mark;
        }

        public string Subject { get; set; }
        public int Mark { get; set; }
    }
}
