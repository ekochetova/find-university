﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Newtonsoft.Json;

namespace FindUniversity
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private string pathCourses = "../../../../data/courses.json";
        private string pathSubjects = "../../../../data/subjects.json";

        private ObservableCollection<string> subjects;
        List<ViewCourse> passedCourses = new List<ViewCourse>();
        List<Course> courses = new List<Course>();

        public MainWindow()
        {
            InitializeComponent();
            LoadData();

            DataContext = new ViewModel(subjects);
            dataGridCourses.ItemsSource = passedCourses;
        }

        private void textBoxMark_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                var textBox = sender as TextBox;
                if (textBox.Text == "")
                    return;
                int mark;
                if (!int.TryParse(textBox.Text, out mark) || mark < 0 || mark > 100)
                    textBox.Text = textBox.Text.Substring(0, textBox.Text.Length - 1);
                textBox.CaretIndex = textBox.Text.Length;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Неверное значение для балла\n" + ex.Message);
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            int markIndividual = 0;
            List<ExamResult> examResults = new List<ExamResult>();
            try
            {
                var results = stackPanelMarks.Children;
                foreach (var i in results)
                {
                    if ((i as StackPanel).Children.OfType<TextBox>().First().Text != "")
                    {
                        string subject = (i as StackPanel).Children.OfType<ComboBox>().First().SelectedItem.ToString();
                        int mark = int.Parse((i as StackPanel).Children.OfType<TextBox>().First().Text);
                        examResults.Add(new ExamResult(subject, mark));
                    }
                }
                if (textBoxMarkIndividual.Text != "")
                    markIndividual = int.Parse(textBoxMarkIndividual.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка при получении баллов\n" + ex.Message);
            }

            try
            {
                passedCourses.Clear();
                foreach (var i in courses)
                {
                    bool ok = true;
                    int sum = 0;
                    foreach (var j in i.Subjects)
                    {
                        if (examResults.Select(x => x.Subject).Contains(j))
                        {
                            sum += examResults.First(x => x.Subject == j).Mark;
                        }
                        else
                        {
                            ok = false;
                        }
                    }
                    if (ok && sum >= i.Mark)
                        passedCourses.Add(new ViewCourse(i));
                }
                dataGridCourses.Items.Refresh();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка при загрузке направлений\n" + ex.Message);
            }
        }

        private void LoadData()
        {
            try
            {
                subjects = JsonConvert.DeserializeObject<ObservableCollection<string>>(File.ReadAllText(pathSubjects));
                courses = JsonConvert.DeserializeObject<List<Course>>(File.ReadAllText(pathCourses));
            }
            catch (Exception ex)
            {
                MessageBox.Show("Произошла ошибка при загрузке файла!\n" + ex.Message);
                Close();
            }
        }

    }
}
