﻿using FindUniversity;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FindUniversity_AddCourses
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private string pathCourse = "../../../../data/courses.json";
        List<Course> Courses { get; set; } = new List<Course>();

        public MainWindow()
        {
            InitializeComponent();
            LoadCourses();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                List<string> subjects = new List<string>();
                foreach (var i in stackPanelSubjects.Children)
                {
                    string subject = (i as TextBox).Text;
                    if (subject != "")
                    subjects.Add(subject);
                }
                Courses.Add(new Course()
                {
                    Name = textBoxName.Text,
                    Mark = int.Parse(textBoxMark.Text),
                    Subjects = subjects,
                });
                SaveCourses();
                MessageBox.Show("Добавлено.");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void LoadCourses()
        {
            Courses = JsonConvert.DeserializeObject<List<Course>>(File.ReadAllText(pathCourse));
        }

        private void SaveCourses()
        {
            File.WriteAllText(pathCourse, JsonConvert.SerializeObject(Courses, Formatting.Indented,
                                                        new JsonConverter[] { new StringEnumConverter() }));
        }
    }
}
